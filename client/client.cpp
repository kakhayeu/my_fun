#include "client.h"


UdpClient::UdpClient(QObject *obj) : QObject(obj) {
  m_pudp = new QUdpSocket(this);
  m_pudp->bind(2424); //default port
  connect(m_pudp, SIGNAL(readyRead()), SLOT(slotProcessDatagrams()));
}

void UdpClient::slotProcessDatagrams() {
  QByteArray baDatagram;
  do {
    baDatagram.resize(m_pudp->pendingDatagramSize());
    m_pudp->readDatagram(baDatagram.data(), baDatagram.size());
  } while (m_pudp->hasPendingDatagrams());

  QDataStream in(&baDatagram, QIODevice::ReadOnly);

  clearBuffer();

  while (!in.atEnd()) {
    in >> received_value_;
    received_values_.push_back(received_value_);
  }
}

QVector<double> UdpClient::getReceivedValues() const {
  return received_values_;
}

void UdpClient::clearBuffer(){
    received_values_.clear();
}
