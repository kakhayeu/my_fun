#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include "client.h"
#include "lib/axistag.h"
#include "lib/qcustomplot.h"
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

private:
  Ui::MainWindow *ui;
  QTimer *playBackTimer;
  QElapsedTimer *timer;
  UdpClient *client;

  QCustomPlot *mPlot;
  QPointer<QCPGraph> mGraph1;
  AxisTag *mTag1;
  uint8_t iteration_counter;

private slots:
  void timerSlot();
};
#endif // MAINWINDOW_H
