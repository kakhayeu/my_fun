#include <QObject>
#include <QTextEdit>
#include <QTimer>
#include <QUdpSocket>
//#include <QDebug>

class UdpClient : public QObject {
  Q_OBJECT

public:
  UdpClient(QObject *obj = 0);
  QVector<double> getReceivedValues() const;
  void clearBuffer();

private:
  QUdpSocket *m_pudp;
  double received_value_;
  QVector<double> received_values_;

private slots:
  void slotProcessDatagrams();
};
