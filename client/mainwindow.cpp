#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow), mPlot(nullptr),
      mTag1(nullptr), iteration_counter(0) {
  ui->setupUi(this);
  playBackTimer = new QTimer(this);
  client = new UdpClient(this);
  mPlot = new QCustomPlot(this);
  timer = new QElapsedTimer;

  setCentralWidget(mPlot);

  // configure plot:
  QSharedPointer<QCPAxisTickerTime> timeTicker(new QCPAxisTickerTime);
  timeTicker->setTimeFormat("%h:%m:%s");
  mPlot->xAxis->setTicker(timeTicker);

  mPlot->yAxis->setTickLabels(true);
  mPlot->yAxis->setRange(-1.2, 1.2);
  connect(mPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), mPlot->yAxis,
          SLOT(setRange(QCPRange)));

  mPlot->axisRect()->addAxis(QCPAxis::atRight);
  mPlot->axisRect()->axis(QCPAxis::atRight, 1)->setPadding(30);

  mGraph1 = mPlot->addGraph(mPlot->xAxis,
                            mPlot->axisRect()->axis(QCPAxis::atRight, 1));
  mGraph1->setPen(QPen(QColor(250, 120, 0)));

  mTag1 = new AxisTag(mGraph1->valueAxis());
  mTag1->setPen(mGraph1->pen());
  timer->start();
  connect(playBackTimer, SIGNAL(timeout()), this, SLOT(timerSlot()));
  playBackTimer->start(40); // 25 fps. do not edit!
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::timerSlot() {
  //  static QTime time(QTime::currentTime());

  double key = timer->elapsed();
  uint8_t time_ratio = 120 / 40; // 120 according to server settings
  uint8_t corr_ratio = client->getReceivedValues().size() / time_ratio;

  QVector<double> xAxis_data(corr_ratio);
  QVector<double> yAxis_data(corr_ratio);
  for (int i = 0; i < corr_ratio; i++) {
    xAxis_data[i] = key + i * (40 / corr_ratio);
    yAxis_data[i] = client->getReceivedValues()[iteration_counter + i];
  }
  if (iteration_counter < time_ratio) {
    iteration_counter += corr_ratio;
  } else {
    iteration_counter = 0;
    client->clearBuffer();
  }

  mGraph1->addData(xAxis_data, yAxis_data);
  mPlot->xAxis->rescale();

  mGraph1->rescaleValueAxis(false, true);
  mPlot->xAxis->setRange(mPlot->xAxis->range().upper, 3000, Qt::AlignRight);

  double graph1Value = mGraph1->dataMainValue(mGraph1->dataCount() - 1);

  mTag1->updatePosition(graph1Value);
  mTag1->setText(QString::number(graph1Value, 'f', 3));
  mPlot->replot();
}
