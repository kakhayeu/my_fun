#include <QElapsedTimer>
#include <QObject>
#include <QRandomGenerator>
#include <QTextEdit>
#include <QTime>
#include <QTimer>
#include <QUdpSocket>
#include <QtMath>

class UdpServer : public QObject {
  Q_OBJECT

public:
  UdpServer(QString, quint16);

private:
  QUdpSocket *m_pudp;
  QString ipAddress_;
  QTimer *ptimer;
  quint16 port_;

private slots:
  void slotSendDatagram();
};
