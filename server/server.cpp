#include "server.h"
#include <QTime>

#define MAX_DATAGRAM_SIZE 480

UdpServer::UdpServer(QString ipAddress, quint16 port) {
  ipAddress_ = ipAddress;
  port_ = port;
  m_pudp = new QUdpSocket(this);
  ptimer = new QTimer(this);

  ptimer->setInterval(120);
  ptimer->start();

  connect(ptimer, SIGNAL(timeout()), SLOT(slotSendDatagram()));
}

void generate(QDataStream &stream) {

  static QTime time(QTime::currentTime());

  double key = time.elapsed() / 1000.0; // time elapsed since start, in seconds

  for (size_t i = 0; i < MAX_DATAGRAM_SIZE / sizeof(double); i++) {
    stream << qSin(key) + qrand() / (double)RAND_MAX * 1 * qSin(key / 0.3843);
  }
}

void UdpServer::slotSendDatagram() {
  QByteArray baDatagram;
  QDataStream out(&baDatagram, QIODevice::WriteOnly);
  generate(out);
  m_pudp->writeDatagram(baDatagram, QHostAddress(ipAddress_), port_);
}
