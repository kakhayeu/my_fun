#include "server.h"
#include <QApplication>
#include <iostream>

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  QString address;
  quint16 port;

  if (argc != 3) {
    std::cout << "Address and/or port has not been entered. \nUsing localhost "
                 "and default port 2424"
              << std::endl;
    address = "127.0.0.1";
    port = 2424;
  } else {
    address = argv[1];
    port = *argv[2];
  }

  UdpServer server(address, port);

  return a.exec();
}
