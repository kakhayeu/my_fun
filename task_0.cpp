#include <bitset>

uint16_t byteswap(uint16_t input) {
    uint16_t result = input << 8 | input >> 8;
    return result;
}
