#include <iostream>
#include <bitset>

bool isValid(uint32_t mask)
{
    for (;mask != 0; mask <<=1) {
        if ((mask & (1<<31)) == 0)
            return false; // Highest bit is now zero, but mask is non-zero.
    }
    return true; // Mask was, or became 0.
}

uint32_t subnet_addr(uint32_t ip_addr, uint32_t mask) {
  
  if (isValid(mask)){
	 return	ip_addr |= ~mask; 
	  }else{
		  std::cout << "Entered mask is invalid" << std::endl;
		return 0;   
		}
}

int main() {
  // ip_addr = 203.0.113.36
  uint32_t ip_addr = 0b11001011'00000000'01110001'00100100;
  // mask = 255.255.255.240
  uint32_t mask = 0b11111111'11111111'11111111'11110000;
  std::cout << std::bitset<sizeof(uint32_t) * 8>(subnet_addr(ip_addr, mask))
            << std::endl;
  return 0;
}
