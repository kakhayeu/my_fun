TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        edge.cpp \
        main.cpp \
        point.cpp \
        polygon.cpp

HEADERS += \
    edge.h \
    point.h \
    polygon.h
