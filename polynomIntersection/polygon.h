#ifndef POLYGON_H
#define POLYGON_H
#include "edge.h"
#include "point.h"
#include <vector>

struct polygon {
  polygon(point first, point second, point third);
  void addPoint(point next);
  std::vector<edge> object;

private:
  polygon() = delete;
};

#endif // POLYGON_H
