#include "polygon.h"

polygon::polygon(point first, point second, point third) {
  object.push_back({first, second});
  object.push_back({second, third});
  object.push_back({third, first});
}

void polygon::addPoint(point next) {
  object.pop_back();
  object.push_back({object.back().second, next});
  object.push_back({next, object.front().first});
}
