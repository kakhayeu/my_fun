#ifndef POINT_H
#define POINT_H
#include <cmath>

enum { LEFT, RIGHT, BEYOND, BEHIND, BETWEEN, ORIGIN, DESTINATION };

struct point {
  float x;
  float y;
  float length();
  point operator-(const point &p) const;
  int operator==(point &p) const;
  int classify(const point &p0, const point &p1) const;
};

#endif // POINT_H
