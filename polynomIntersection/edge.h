#ifndef EDGE_H
#define EDGE_H
#include "point.h"

struct edge {
  point first;
  point second;
  edge(point f, point s);

private:
  edge() = delete;
};

#endif // EDGE_H
