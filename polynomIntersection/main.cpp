#include "edge.h"
#include "point.h"
#include "polygon.h"
#include <cmath>
#include <iostream>
#include <vector>

enum { TOUCHING, CROSSING, INESSENTIAL };

bool edgesAreIntersecting(const edge &a, const edge &b) {
  float v1 = (b.second.x - b.first.x) * (a.first.y - b.first.y) -
             (b.second.y - b.first.y) * (a.first.x - b.first.x);
  float v2 = (b.second.x - b.first.x) * (a.second.y - b.first.y) -
             (b.second.y - b.first.y) * (a.second.x - b.first.x);
  float v3 = (a.second.x - a.first.x) * (b.first.y - a.first.y) -
             (a.second.y - a.first.y) * (b.first.x - a.first.x);
  float v4 = (a.second.x - a.first.x) * (b.second.y - a.first.y) -
             (a.second.y - a.first.y) * (b.second.x - a.first.x);
  return ((v1 * v2) < 0) && ((v3 * v4) < 0);
}

int edgeType(const point &p, const edge &e) {
  point v = e.first;
  point w = e.second;
  switch (p.classify(e.first, e.second)) {
  case LEFT:
    return ((v.y < p.y) && (p.y <= w.y)) ? CROSSING : INESSENTIAL;
  case RIGHT:
    return ((w.y < p.y) && (p.y <= v.y)) ? CROSSING : INESSENTIAL;
  case BETWEEN:
  case ORIGIN:
  case DESTINATION:
    return TOUCHING;
  default:
    return INESSENTIAL;
  }
}

bool pointInPolygon(const point &p, const polygon &pol) {
  int parity = 0;
  for (size_t i = 0; i < pol.object.size(); i++ /*, p.advance (CLOCKWISE)*/) {
    edge e = pol.object[i];
    switch (edgeType(p, e)) {
    case TOUCHING:
      return true;
    case CROSSING:
      parity = 1 - parity;
    }
  }
  return (parity ? true : false);
}

bool polyIsInside(const polygon &pA, const polygon &pB) {
  point pA_first = pA.object[0].first;
  point pB_first = pB.object[0].first;

  return (pointInPolygon(pA_first, pB) || pointInPolygon(pB_first, pA) ? true
                                                                       : false);
};

bool polygonsAreIntersecting(const polygon &pA, const polygon &pB) {
  for (size_t i = 0; i <= pA.object.size(); i++) {
    for (auto a : pA.object) {
      if (edgesAreIntersecting(a, pB.object[i])) {
        return true;
      }
    }
  }
  return polyIsInside(pA, pB);
}

int main() {
  // figures are not intersecting
  polygon fig_1({2, 6}, {4, 3}, {2, 0});
  fig_1.addPoint({0, 3});
  polygon fig_2({3, 5}, {3, 7}, {5, 7});
  fig_2.addPoint({5, 5});
  std::cout << polygonsAreIntersecting(fig_1, fig_2) << std::endl;

  // figures are intersecting
  polygon fig_3({2, 6}, {4, 3}, {2, 0});
  fig_3.addPoint({0, 3});
  polygon fig_4({3, 7}, {5, 7}, {5, 5});
  fig_4.addPoint({3, 3});
  std::cout << polygonsAreIntersecting(fig_3, fig_4) << std::endl;

  // figures fig_5 contains fig_6
  polygon fig_5({0, 0}, {5, 0}, {5, 5});
  fig_5.addPoint({0, 5});
  polygon fig_6({1, 1}, {2, 1}, {2, 2});
  fig_6.addPoint({1, 2});
  std::cout << polygonsAreIntersecting(fig_5, fig_6) << std::endl;

  return 0;
}
