#include "point.h"

float point::length() { return sqrt(x * x + y * y); }

point point::operator-(const point &p) const {
  return point({x - p.x, y - p.y});
}

int point::operator==(point &p) const { return (x == p.x) && (y == p.y); }

int point::classify(const point &p0, const point &p1) const {
  point p2 = *this;
  point a = p1 - p0;
  point b = p2 - p0;
  float sa = a.x * b.y - b.x * a.y;
  if (sa > 0.0)
    return LEFT;
  if (sa < 0.0)
    return RIGHT;
  if ((a.x * b.x < 0.0) || (a.y * b.y < 0.0))
    return BEHIND;
  if (a.length() < b.length())
    return BEYOND;
  if (p0 == p2)
    return ORIGIN;
  if (p1 == p2)
    return DESTINATION;
  return BETWEEN;
}
