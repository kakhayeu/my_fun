#include <bitset>

uint32_t up_to_pow2(uint32_t v) {
    unsigned int r = 0;

    while (v >>= 1) {
        r++;
    }
    return r;
}
